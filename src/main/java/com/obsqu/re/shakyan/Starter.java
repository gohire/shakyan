package com.obsqu.re.shakyan;

import java.awt.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Starter {

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static void main(String[] argv) throws AWTException {
        RobotThread robotThread = new RobotThread();
        scheduler.scheduleAtFixedRate(robotThread, 3, 3, TimeUnit.MINUTES);
    }
}
