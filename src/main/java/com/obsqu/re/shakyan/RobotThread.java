package com.obsqu.re.shakyan;

import java.awt.*;

public class RobotThread implements Runnable {

    private Robot robot;
    private final static int WHEEL_AMT = 0;

    RobotThread() throws AWTException {
        this.robot = new Robot();
    }

    @Override
    public void run() {
        robot.mouseWheel(WHEEL_AMT);
    }
}
